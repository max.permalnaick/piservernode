var express = require('express');
var router = express.Router();
const Acquisition = require('../acquisition.js');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});





/**
 *
 *
  /dev/shm
  gpsNmea
  $GPGGA,155221.80,4807.060,N,01131.337,E,1,08,0.9,543.2,M,46.9,M, , *66
  $GPRMC,155221.80,A,4807.060,N,01131.337,W,000.0,054.7,180119,020.3,E*66

  rainCounter.log
  2019-01-18T15:54:16.122Z

  sensors
  {"date":"2019-01-18T15:55:02.402Z","measure":[{"name":"temperature","desc":"Température","unit":"C","value":"10.01"},{"name":"pressure","desc":"Pression","unit":"hP","value":"995.01"},{"name":"humidity","desc":"Humidité","unit":"%","value":"50.1"},{"name":"luminosity","desc":"Luminosité","unit":"Lux","value":"48509"},{"name":"wind_heading","desc":"Direction du vent","unit":"°","value":"190.301178541937"},{"name":"wind_speed_avg","desc":"Force moyenne du vent","unit":"Kts","value":"39.7"},{"name":"wind_speed_max","desc":"Force maxi du vent","unit":"Kts","value":"55.2"},{"name":"wind_speed_min","desc":"Force moyenne du vent","unit":"Kts","value":"24.5"}]}

  tph.log
  {"date":2019-01-18T16:55:24.283269 ,"temp":-74.977,"hygro":15.736,"press":1011.518}


  all, press, temp, hygro, lum, wind_dir, wind_mean, wind_min, wind_max, location, rain

  [IP]:80/last?capteur_type=[type]
  [IP]:80/period?capteur_type=[type]&dateStart=[date]&dateEnd=[date]

  date
  temp
  hygro
  press
  lum
  wind_dir
  wind_mean
  wind_min
  wind_max
  lat
  long
  alt
  rain
  curl 'http://localhost:3000/last?capteur_type=all'
*/
router.get('/last', function (req, res, next)
{
  const acquisition = new Acquisition();

  try
  {
    // console.log(req);
    if (!req.query.capteur_type)
      throw "variable non définie !";

    let nosDemandes = req.query.capteur_type.split(",");
    let err;
    if(!acquisition.matchQueries(nosDemandes,err))
      throw "variable non définie : " + err;

      acquisition.queryLast(req,res);
    
  }
  catch (error)
  {
    console.log("!!!!!!!!!!!!!!!!!!!!!! ERR : " + error);
    res.status(500).send({"error":error});
  }
});









/**
 * curl 'http://localhost:3000/period?capteur_type=all&dateStart=1547924400&dateEnd=1548442800'
 */
router.get('/period', function (req, res, next)
{
  const acquisition = new Acquisition();

  try
  {
    // console.log(req);
    if (!req.query.capteur_type)
      throw "variables non définies";

    if (!(req.query.dateStart && req.query.dateEnd))
      throw "pas de période définie";

    let nosDemandes = req.query.capteur_type.split(",");
    let err;
    if(!acquisition.matchQueries(nosDemandes,err))
      throw "variable non définie : " + err;

      acquisition.queryPeriod(req, res);

  }
  catch (error)
  {
    console.log("!!!!!!!!!!!!!!!!!!!!!! ERR : " + error);
    res.status(500).send({"error":error});
  }
});




module.exports = router;