
const Influx = require('influx');
const Nmea = require("node-nmea");
const fs = require('fs');



/**
 * @brief	Classe pour gérer le serveur sur le raspberry.
 * 			Elle permet de lire les données (toutes les secondes) d'acquisition du controleur via les fichiers :
 * 			"/dev/shm/sensors", "/dev/shm/tph.log", "/dev/shm/gpsNmea", "/dev/shm/rainCounter.log".
 * 			**comportement client** : elle enregistre ces données dans une base de donnée influx sur le port 8086 => influxd doit être exécutée avant le lancement du programme
 * 			**comportement serveur**: elle gère les requêtes GET sur le port 3000 en CORS.
 * 
 * @note	La classe gère le traitement des mesures capteurs de rain counter => une table pour capteurs et une autre pour rain
 * 			Plus simple à mettre en place au niveau des méthodes (séparées)
 * 
 * @url 	https://www.influxdata.com/blog/getting-started-with-node-influx/
 * 			https://docs.influxdata.com/influxdb/v1.5/introduction/getting-started/
 */
module.exports = class Acquisition {
	// "localhost:8086"  "piensg009:8086"  "172.31.43.62:8086"
	constructor(_hostname = "localhost:8086", _dbname = 'raspi_capteurs', _tablename = 'capteurs', _tablerains = 'rains') {
		this.THIS = "AcquisitionCapteurs"
		this.ID = "009";
		this.NAME = "SONDE_MCM";
		this.hostname = _hostname;
		this.dbname = _dbname;
		this.tablename = _tablename;
		this.TAGS = [
			"id",
			"name",
			"date", "temp", "hygro", "press",
			"lum", "wind_dir", "wind_mean", "wind_min", "wind_max",
			"dateloc", "location", "all"];
		this.raspiDev = ["/dev/shm/sensors", "/dev/shm/tph.log", "/dev/shm/gpsNmea", "/dev/shm/rainCounter.log"];
		this.influx = new Influx.InfluxDB({
			host: this.hostname,
			database: this.dbname,
			schema: [
				{
					measurement: this.tablename,
					fields: {
						id: Influx.FieldType.STRING,
						name: Influx.FieldType.STRING,
						date: Influx.FieldType.STRING,
						temp: Influx.FieldType.FLOAT,
						hygro: Influx.FieldType.FLOAT,
						press: Influx.FieldType.FLOAT,
						lum: Influx.FieldType.FLOAT,
						wind_dir: Influx.FieldType.FLOAT,
						wind_mean: Influx.FieldType.FLOAT,
						wind_min: Influx.FieldType.FLOAT,
						wind_max: Influx.FieldType.FLOAT,
						dateloc: Influx.FieldType.STRING,
						lat: Influx.FieldType.STRING,
						long: Influx.FieldType.STRING,
						alt: Influx.FieldType.FLOAT,
					},
					tags: this.TAGS
				}
			]
		});

		this.TAGSrain = [
			"id",
			"name",
			"rain"];
		this.tablerains = _tablerains;
		this.influxRain = new Influx.InfluxDB({
			host: this.hostname,
			database: this.dbname,
			schema: [
				{
					measurement: this.tablename,
					fields: {
						id: Influx.FieldType.STRING,
						name: Influx.FieldType.STRING,
						rain: Influx.FieldType.STRING
					},
					tags: this.TAGSrain
				}
			]
		});

		// console.log(this.getdbname());
		// console.log(this.gettable());
	}



	/**
	 * @brief 	la méthode récursive qui lit les données toutes les secondes
	 * 			Si la base n'existe pas elle le crée, elle y enregistre les données.
	 */
	run() {
		this._acquire1Point();
		setTimeout(this.run.bind(this), 60000);
	}



	/**
	 * @brief 	Gère la requête /last?capteur_type=xxx.
	 * 			Retourne 200 en cas de réussite avec le json
	 * 			sinon retourne 404 en cas d'échec
	 * @param {*} req variable requête
	 * @param {*} res variable réponse
	 */
	queryLast(req, res) {
		// console.log("===>capteur_type " + req.query.capteur_type);
		if (req.query.capteur_type.includes('all')) {
			this._queryLastAll(res);
		}
		else if (req.query.capteur_type.includes('rain')) {
			this._queryLastRain(res);
		}
		else {
			this._queryLastDefault(req, res);
		}
	}



	/**
	 * @brief 	Gère la requête /period?capteur_type=XXX&dateStart=SSS&dateEnd=EEE
	 * 			Retourne 200 en cas de réussite avec le json
	 * 			sinon retourne 404 en cas d'échec
	 * @param {*} req variable requête
	 * @param {*} res variable réponse
	 */
	queryPeriod(req, res) {
		// console.log("===>capteur_type " + req.query.capteur_type);
		let start = new Date(Number(req.query.dateStart) * 1000).toISOString();
		let end = new Date(Number(req.query.dateEnd) * 1000).toISOString();

		if (req.query.capteur_type.includes('all')) {
			this._queryPeriodAll(res, start, end);
		}
		else if (req.query.capteur_type.includes('rain')) {
			this._queryPeriodRain(res, start, end);
		}
		else {
			this._queryPeriodDefault(req, res, start, end);
		}
	}



	/**
	 * @brief	Vérifie si la requête est acceptée ou pas
	 * @param {*} nosDemandes requête split(,)
	 * @param {*} err devrait retourner la variable non prise en charge
	 */
	matchQueries(nosDemandes, err) {
		let tab = this.TAGS;
		tab.push("rain");
		for (let d = 0; d < nosDemandes.length; d++) {
			err = nosDemandes[d];
			let bfind = false;
			for (let t = 0; t < tab.length; t++) {
				let element = tab[t];
				// console.log(err,element);
				if (err === element) {
					bfind = true; break;
				}
			}
			if (!bfind) {
				console.error("Variable non prise en charge : ", err);
				return false;
			}
		}
		return true;
	}






















	/********************************************************************************************************
	*
	*												METHODES PRIVEES(ACQUISITION)
	*
	********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////
	/// ECRITURE DES DONNEES

	/**
	 * @brief méthode private qui grab un jeu de données et les écrit dans la db
	 */
	_acquire1Point() {
		// console.log("=====>vérification db");
		this.influx.getDatabaseNames()
			.then(names => {
				if (!names.includes(this.dbname)) {
					console.log((new Date()).toISOString() + "=====>création db");
					return this.influx.createDatabase(this.dbname);
				}
			})
			.then(() => {
				this._writeData();
			})
			.catch(error => console.error(error));
	}

	/**
	 * @brief 	cette méthode privée écrit les données dans la db, elle lit les fichiers en promise
	 * 			puis après un promiseall écrit dans la db via influx API
	 */
	_writeData() {
		let sensorsJson = [];

		// console.log("===>Les fichiers à traiter : " + this.raspiDev);
		let nosPromises = this._readFilesInPromise(this.raspiDev);

		Promise.all(nosPromises)
			.then(_promises => {

				_promises.forEach(p => {
					sensorsJson.push(p);
					// console.log(p)
				})

				let myDatas = this._formatData(sensorsJson);
				// console.log(myDatas);

				this.influx.writePoints(myDatas.capt, { database: this.dbname, precision: 's', })
					//.then(() => console.log((new Date()).toISOString() + " : écriture dans database"))
					.catch(error => console.error("Erreur dans l'écriture de la database !", this.tablename, error));

				this.influxRain.writePoints(myDatas.rain, { database: this.dbname, precision: 's', })
					//.then(() => console.log((new Date()).toISOString() + " : écriture dans database"))
					.catch(error => console.error("Erreur dans l'écriture de la database !", this.tablerains, error));
			});
	}

	/**
	 * @brief cette méthode privée met les fichiers en paramètre en lecture via des promises
	 * @param {*} _files 
	 * @return retourne les promises
	 */
	_readFilesInPromise(_files) {
		let nosPromises = [];
		_files.forEach(file => {
			// console.log("====>fichier à traiter : " + file);
			nosPromises.push(new Promise((resolve, reject) => {
				fs.readFile(file, (err, data) => {
					if (err) reject(err);
					else resolve(String(data));
				})
			}));
		});
		return nosPromises;
	}

	/**
	 * format les données en JSON pour les capteurs et les rains
	 * @param {tableau} sensorsJson 
	 */
	_formatData(sensorsJson) {
		//  console.log(sensorsJson);
		const tSensors = JSON.parse(sensorsJson[0]);
		const tTph = JSON.parse(sensorsJson[1]);
		const jsonGps = this._nmeaParser(sensorsJson[2]);
		const jsonRain = ("" + sensorsJson[3]).trim();

		if (!jsonGps.valid) console.log("====> ERR : Coordonnées GPS non valides");
		//console.log(new Date(tSensors.date));
		//console.log(jsonGps.datetime);

		// console.log(" TIMESTAMP SIMULE")
		let myDatas =
			[
				{
					measurement: this.tablename,
					precision: "s",
					timestamp: new Date(tSensors.date),
					fields: {
						id: this.ID,
						name: this.NAME,
						date: new Date(tSensors.date).toISOString(),
						temp: tTph.temp,
						press: tTph.press,
						hygro: tTph.hygro,
						lum: tSensors.measure[3].value,
						wind_dir: tSensors.measure[4].value,
						wind_mean: tSensors.measure[5].value,
						wind_max: tSensors.measure[6].value,
						wind_min: tSensors.measure[7].value,
						dateloc: (!jsonGps.valid) ? "" : new Date(jsonGps.datetime).toISOString(),
						lat: (!jsonGps.valid) ? "" : Nmea.degToDec(jsonGps.loc.dmm.latitude),
						long: (!jsonGps.valid) ? "" : Nmea.degToDec(jsonGps.loc.dmm.longitude),
						alt: (!jsonGps.valid) ? "" : jsonGps.loc.dmm.alt,
					},
				}
			];

		// console.log(" TIMESTAMP SIMULE")
		let myRainDatas =
			[
				{
					measurement: this.tablerains,
					precision: "s",
					timestamp: new Date(jsonRain),
					fields: {
						id: this.ID,
						name: this.NAME,
						rain: new Date(jsonRain).toISOString(),
					},
				}
			];
		return { capt: myDatas, rain: myRainDatas };
	}

	_nmeaParser(_trameGpsNmea) {
		let splits = _trameGpsNmea.split('\n');
		let data = Nmea.parse(splits[1]);
		// console.log(data);
		return data;
	}
	
	/********************************************************************************************************
	*
	*												METHODES PRIVEES (QUERY)
	*
	********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////
	/// REFACTOR DE QUERY LAST

	_queryLastDefault(req, res) {

		let monType = this._formatQuerySQL(req.query.capteur_type);
		let requete = "select " + monType + " from " + this.tablename + " ORDER BY time DESC LIMIT 1";
		console.log(requete);
		this.influx.query(requete)
			.then(result => {
				// console.log(result[0]);
				let newJson = this._formatJsonLast(result[0]);
				// console.log(newJson);
				res.status(200).send(newJson);
			})
			.catch(error => { console.error(error); res.status(404).send(error); });
	}

	_queryLastRain(res) {
		let requete = "select rain from " + this.tablerains + " ORDER BY time DESC LIMIT 1";
		console.log(requete);
		this.influxRain.query(requete)
			.then(result => {
				// console.log(result[0]);
				let newJson = this._formatJsonLastRain(result[0]);
				// console.log(newJson);
				res.status(200).send(newJson);
			})
			.catch(error => { console.error(error); res.status(404).send(error); });
	}

	_queryLastAll(res) {
		let requete1 = "select * from " + this.tablename + " ORDER BY time DESC LIMIT 1";
		let requete2 = "select rain from " + this.tablerains + " ORDER BY time DESC LIMIT 1";
		console.log(requete1);
		console.log(requete2);
		this.influx.query(requete1).then(result => this.influxRain.query(requete2)
			.then(result2 => {
				// console.log(result[0], result2[0]);
				let newJson = this._formatJsonLastAll(result[0], result2[0]);
				// console.log(newJson);
				res.status(200).send(newJson);
			}))
			.catch(error => { console.error(error); res.status(404).send(error); });
	}
	
	////////////////////////////////////////////////////////////////////////////
	/// REFACTOR DE QUERY PERIOD

	_queryPeriodAll(res, start, end) {
		let requeteMes = "select * from " + this.tablename + " where time>'" + start + "' AND time<'" + end + "' order by time";
		let requeteRain = "select rain from " + this.tablerains + " where time>'" + start + "' AND time<'" + end + "' order by time";
		console.log(requeteMes);
		console.log(requeteRain);

		this.influx.query(requeteMes).then(resultMes => this.influxRain.query(requeteRain)
			.then(resultRain => {
				//console.log(resultRain, resultRain.length);
				let arrRain = [];
				for (let i = 0; i < resultRain.length; i++) {
					//console.log(resultRain[i]);
					arrRain.push(this._formatJsonPeriodRain(resultRain[i]));
				}
				//console.log(resultMes, resultMes.length);
				let arrMes = [];
				for (let i = 0; i < resultMes.length; i++) {
					//console.log(resultMes[i]);
					arrMes.push(this._formatJsonPeriod(resultMes[i]));
				}
				let trame = { id: this.ID, name: this.NAME, dataLength : arrMes.length, data: arrMes, rainLength:arrRain.length ,rain: arrRain };
				console.log("trame nb elements : ", arrRain.length, arrMes.length);
				res.status(200).send(trame);
			}))
			.catch(error => { console.error(error); res.status(404).send(error); });
	}

	_queryPeriodRain(res, start, end) {
		let requeteRain = "select rain from " + this.tablerains + " where time>'" + start + "' AND time<'" + end + "' order by time";
		console.log(requeteRain);

		this.influxRain.query(requeteRain)
			.then(resultRain => {
				//console.log(resultRain, resultRain.length);
				let arrRain = [];
				for (let i = 0; i < resultRain.length; i++) {
					console.log(resultRain[i]);
					arrRain.push(resultRain[i].rain);
				}
				let trame = { id: this.ID, name: this.NAME, rainLength:arrRain.length, rain: arrRain };
				console.log("trame nb elements : ", arrRain.length);
				res.status(200).send(trame);
			})
			.catch(error => { console.error(error); res.status(404).send(error); });
	}

	_queryPeriodDefault(req, res, start, end) {
		let monType = this._formatQuerySQL(req.query.capteur_type);
		let requeteMes = "select " + monType + " from " + this.tablename + " where time>'" + start + "' AND time<'" + end + "' order by time";
		console.log(requeteMes);

		this.influx.query(requeteMes)
			.then(resultMes => {
				//console.log(resultMes, resultMes.length);
				let arrMes = [];
				for (let i = 0; i < resultMes.length; i++) {
					//console.log(resultMes[i]);
					arrMes.push(this._formatJsonPeriod(resultMes[i]));
				}

				let trame = { id: this.ID, name: this.NAME, dataLength : arrMes.length, data: arrMes };
				console.log("trame nb elements : ", arrMes.length);
				res.status(200).send(trame);
			})
			.catch(error => { console.error(error); res.status(404).send(error); });
	}
	
	////////////////////////////////////////////////////////////////////////////
	/// FORMATAGE DE DONNEES

	_formatQuerySQL(req) {
		let temp = req;
		if (req.includes("location")) {
			temp = req.replace("location", "lat,long,alt,dateloc");
			//console.log("variables SQL",temp));
		}
		return temp+",date";
	}

	_formatJsonLastAll(_res, _res2) {
		let newJson = {
			"id": this.ID,
			"name": this.NAME,
			"location": {
				"lat": _res.lat,
				"lng": _res.long,
				"date": _res.dateloc
			},
			"measurements": {
				"date": _res.date,
				"press": _res.press,
				"temp": _res.temp,
				"hygro": _res.hygro,
				"lum": _res.lum,
				"wind_min": _res.wind_min,
				"wind_max": _res.wind_max,
				"wind_mean": _res.wind_mean,
				"wind_dir": _res.wind_dir,
			},
			"rain": _res2.rain
		};
		return newJson;
	}

	_formatJsonLast(_res) {
		let newJson = {
			"id": this.ID,
			"name": this.NAME,
			"measurements": {
				"date": _res.date,
				"press": _res.press,
				"temp": _res.temp,
				"hygro": _res.hygro,
				"lum": _res.lum,
				"wind_min": _res.wind_min,
				"wind_max": _res.wind_max,
				"wind_mean": _res.wind_mean,
				"wind_dir": _res.wind_dir,
			},
			"location": {
				"lat": _res.lat,
				"lng": _res.long,
				"date": _res.dateloc
			}
		};
		return newJson;
	}

	_formatJsonLastRain(_res) {
		let newJson = {
			"id": this.ID,
			"name": this.NAME,
			"rain": _res.rain
		};
		return newJson;
	}

	_formatJsonPeriod(line) {
		let newJson = {
			"location": {
				"lat": line.lat,
				"lng": line.long,
				"date": line.dateloc
			},
			"measurements": {
				"date": line.date,
				"press": line.press,
				"temp": line.temp,
				"hygro": line.hygro,
				"lum": line.lum,
				"wind_min": line.wind_min,
				"wind_max": line.wind_max,
				"wind_mean": line.wind_mean,
				"wind_dir": line.wind_dir,
			}
		};
		return newJson;
	}

	_formatJsonPeriodRain(line) {
		return { "rain": line.rain };
	}
};
